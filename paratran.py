from shinjuku.transfer import synthesise_things

if __name__ == '__main__':

    from sys import argv

    if not (3 <= len(argv) <= 4):
        print("Usage: python paratran.py shinjuku/triples.txt apgcodes.txt [outfile.sjk]")
        exit(1)

    synthesise_things(*(argv[1:]))
